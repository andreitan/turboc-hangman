#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<graphics.h>
#include<dos.h>

// Everything will be in graphics mode. Everything.

// A day of madness with this pointer-array concept.
FILE *scores[3]; // High score files for each difficulty
FILE *lex[3]; // Dictionary files for each difficulty

void intro(), game(int diff); // Intro, main game
void menu(), chdiff(), scomenu(), help(); // Game menus
void drawbg(int color), drawtitle(int color); // Draw menu's background and title

void newlex(FILE **lex, char file[], int diff); // Create a lex database
void openlex(FILE **lex, char file[]); // Open a lex database

int lexcount[3] = {0, 0, 0}; // Number of available words/phrases per lexicon

const char maker[] = "MADE BY ANDREI TAN", bgi[] = "C:\\TURBOC3\\BGI"; // For easy config
const char *swrong[4] = {
	"Score file",
	"cannot be read",
	"and must be",
	"fixed or erased."
};
const char *lwrong[4] = {
	"Easy lexicon file cannot be read",
	"Intermediate lexicon file cannot be read",
	"Hard lexicon file cannot be read",
	"and must be fixed or recreated. Recreate? [Y/N]"
};
const char *title[10] = { // Array of strings for drawing Hangman
	"�ͻ�ͻ                                       ",
	"� �� �                                       ",
	"� ȼ �����ͻ����ͻ����ͻ�������ͻ����ͻ����ͻ",
	"� ɻ ���ͻ �� ɻ �� ɻ �� ɻ ɻ ���ͻ �� ɻ �",
	"� �� ���ͼ �� �� �� �� �� �� �� ���ͼ �� �� �",
	"� �� �� ɻ �� �� �� �� �� �� �� �� ɻ �� �� �",
	"� �� �� ȼ �� �� �� ȼ �� �� �� �� ȼ �� �� �",
	"�ͼ�ͼ����ͼ�ͼ�ͼ��ͻ ��ͼ�ͼ�ͼ����ͼ�ͼ�ͼ",
	"                  ��ͼ �                     ",
	"                  ����ͼ                     "
};
const char *titleu[10] = { // Array of strings for drawing Hangman's underline
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"����������������ͻ      �������������������ͻ",
	"����������������ͼ      �������������������ͼ"
};

const char *menucho[10] = { // List of menu choices
	"NEW GAME",
	"[NEW GAME]",
	"HIGH SCORES",
	"[HIGH SCORES]",
	"HELP",
	"[HELP]",
	"QUIT",
	"[QUIT]"
};
const char *diffcho[8] = { // List of difficulty choices
	"EASY",
	"[EASY]",
	"INTERMEDIATE",
	"[INTERMEDIATE]",
	"HARD",
	"[HARD]",
	"BACK",
	"[BACK]"
};
const char *scorescho[8] = { // List of scores choices
	"ERASE",
	"[ERASE]",
	"ERASE",
	"[ERASE]",
	"ERASE",
	"[ERASE]",
	"BACK",
	"[BACK]"
};
const char *helptext[10] = { // No spaces after commas due to text rendering
	"Guess the word or phrase by pressing letters on the keyboard.",
	"If the letter is in the word or phrase,",
	"it is shown in all correct positions.",
	"Otherwise,a life is lost. Lose all lives and the game is over.",
	"",
	"The game starts with three lives. An additional life is gained",
	"after every ten successful rounds.",
	"",
	"Press \"Esc\" and \"Y\" in order to return to the menu at any time,",
	"however,your score will be ineligible for the high scores."
};
const char *instr[6] = { // Menu navigation instructions
	"[WASD] to navigate",
	"[ENTER] to select"
};
const char *difftext[3] = { // Difficulty text
	"EASY",
	"INTERMEDIATE",
	"HARD"
};

const char *newlexeasy[30] = { // Default lexeasy in case of null or format break
	"ARCHITECT",
	"DEPOSIT",
	"DEVELOPMENT",
	"ECONOMICS",
	"ENGINEER",
	"FOUNTAIN",
	"GOVERNMENT",
	"HEADQUARTERS",
	"INFORMATION",
	"INSTITUTE",
	"INTERNATIONAL",
	"INVESTIGATE",
	"KNOWLEDGE",
	"LABORATORY",
	"MANUFACTURE",
	"MATHEMATICS",
	"MILITARY",
	"MOUNTAIN",
	"NECESSARY",
	"NOMINATE",
	"PERMANENT",
	"PROGRAMMER",
	"TECHNOLOGY",
	"TOMORROW",
	"TRADITION",
	"TRANSPORTATION",
	"TREATMENT",
	"WITHDRAW",
	"WONDERFUL",
	"YESTERDAY"
};
const char *newlexintermed[30] = { // Default lexintermed in case of null or format break
	"A PIECE OF CAKE",
	"A SLAP ON THE WRIST",
	"ACTIONS SPEAK LOUDER THAN WORDS",
	"ADD FUEL TO THE FIRE",
	"ADD INSULT TO INJURY",
	"AN ARM AND A LEG",
	"AN AXE TO GRIND",
	"BACK AND FORTH",
	"BREAK A LEG",
	"CLOSE BUT NO CIGAR",
	"FLESH AND BLOOD",
	"FOREVER ALONE",
	"FROM RAGS TO RICHES",
	"GREAT MINDS THINK ALIKE",
	"HAPPY BIRTHDAY",
	"I LOVE YOU",
	"KICK THE BUCKET",
	"LET SLEEPING DOGS LIE",
	"OUT OF THE BLUE",
	"PRACTICE MAKES PERFECT",
	"RAINING CATS AND DOGS",
	"RISE AND SHINE",
	"RULE OF THUMB",
	"SAVED BY THE BELL",
	"SHUT UP AND TAKE MY MONEY",
	"SWORD AND SHIELD",
	"THE BEST OF BOTH WORLDS",
	"TWO BIRDS WITH ONE STONE",
	"UPSIDE DOWN",
	"YOU ARE WHAT YOU EAT"
};
const char *newlexhard[30] = { // Default lexhard in case of null or format break
	"ALFALFA",
	"ASTHMA",
	"BUOY",
	"CARIBOU",
	"CROQUET",
	"CZAR",
	"DUPLEX",
	"EGG",
	"GIZMO",
	"GOLF",
	"GYPSY",
	"HAIKU",
	"HERTZ",
	"IVORY",
	"JAZZ",
	"JUICY",
	"KIOSK",
	"LARYNX",
	"PUBLIC",
	"RED",
	"SCHNAPPS",
	"SKILL",
	"STRENGTH",
	"STRETCH",
	"SWING",
	"TWELFTH",
	"WHISKEY",
	"ZEPHYR",
	"ZODIAC",
	"ZULU"
};

int main() {
	int gd = DETECT, gm, i;
	randomize();
	initgraph(&gd, &gm, bgi);
	cleardevice();
	scores[0] = fopen("hmscoeas.txt", "a+t");
	scores[1] = fopen("hmscoint.txt", "a+t");
	scores[2] = fopen("hmscohar.txt", "a+t");
	openlex(&lex[0], "hmlexeas.txt");
	openlex(&lex[1], "hmlexint.txt");
	openlex(&lex[2], "hmlexhar.txt");
	intro();
	menu();
	closegraph();
	nosound();
	fcloseall();
	return(0);
}

void intro() {
	int i;
	cleardevice();
	settextjustify(1, 1);
	settextstyle(SANS_SERIF_FONT, HORIZ_DIR, 2);
	setcolor(RED);
	circle(285, 200, 10);
	setcolor(WHITE);
	setfillstyle(SOLID_FILL, WHITE);
	fillellipse(355, 200, 15, 15);
	setfillstyle(EMPTY_FILL, BLACK);
	delay(150);
	for (i = 0; i < 30; i++) {
		arc(320, 185, 210, 210 + (4 * i), 100);
		delay(5);
	}
	outtextxy(320, 310, maker);
	sound(1046.5);
	delay(200);
	nosound();
	delay(500);
	cleardevice();
}

void menu() { // Main menu
	int choice = 0, i, y;
	char c;
	draw:
	if (choice == 0) {
		drawbg(LIGHTCYAN);
		drawtitle(LIGHTCYAN);
	} else if (choice == 1) {
		drawbg(YELLOW);
		drawtitle(YELLOW);
	} else if (choice == 2) {
		drawbg(LIGHTGREEN);
		drawtitle(LIGHTGREEN);
	} else {
		drawbg(LIGHTRED);
		drawtitle(LIGHTRED);
	}
	settextjustify(1, 2);
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, y = 240; i < 8; i += 2) {
		outtextxy(320, y, menucho[i == choice * 2 ? i + 1 : i]);
		y += textheight(menucho[i]);
	}
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice > 0) {
		choice--;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 3) {
		choice++;
		goto draw;
	} else if (c == 13) {
		sound(82.41);
		delay(125);
		nosound();
		cleardevice();
		if (choice == 0) chdiff();
		else if (choice == 1) scomenu();
		else if (choice == 2) help();
		else return;
		goto draw;
	} else goto choose;
}

void chdiff() { // Difficulty menu
	int choice = 0, i, y;
	char c;
	draw:
	if (choice == 0) {
		drawbg(LIGHTBLUE);
		drawtitle(LIGHTBLUE);
	} else if (choice == 1) {
		drawbg(LIGHTGREEN);
		drawtitle(LIGHTGREEN);
	} else if (choice == 2) {
		drawbg(LIGHTRED);
		drawtitle(LIGHTRED);
	} else {
		drawbg(YELLOW);
		drawtitle(YELLOW);
	}
	settextjustify(1, 2);
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, y = 240; i < 6; i += 2) {
		outtextxy(320, y, diffcho[i == choice * 2 ? i + 1 : i]);
		y += textheight(diffcho[i]);
	}
	y += textheight(diffcho[i]);
	outtextxy(320, y, diffcho[i == choice * 2 ? i + 1 : i]);
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice > 0) {
		choice--;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 3) {
		choice++;
		goto draw;
	} else if (c == 13) {
		sound(82.41);
		delay(125);
		nosound();
		cleardevice();
		if (choice <= 2) game(choice);
		return;
	} else goto choose;
}

void scomenu() { // Show 5 highest scores for each difficulty
	int choice = 3, i, j, x, y, diff, swaps;
	char c, numtext[10];
	unsigned long tempscores[5], temp;
	draw:
	drawbg(choice >= 3 ? YELLOW : LIGHTRED);
	settextjustify(1, 2);
	settextstyle(3, HORIZ_DIR, 1);
	for (diff = 0, x = 140; diff < 3; diff++) {
		if (diff == 0) setcolor(LIGHTBLUE);
		else if (diff == 1) setcolor(LIGHTGREEN);
		else setcolor(LIGHTRED);
		outtextxy(x, 60, difftext[diff]);
		x += 180;
	}
	setcolor(WHITE);
	for (diff = 0, x = 140; diff < 3; diff++) {
		rewind(scores[diff]);
		for (i = 0; fgets(numtext, 10, scores[diff]) != NULL && i < 5; i++) {
			j = sscanf(numtext, "%lu", &tempscores[i]); // Read each line from score file
			if (j <= 0) { // Format break detected
				for (j = 0, y = 60 + (textheight(difftext[diff]) * 2); j < 4; j++) {
					outtextxy(x, y, swrong[j]);
					y += textheight(difftext[diff]);
				}
				goto end;
			}
		}
		do {
			swaps = 0;
			for (j = 0; j < i - 1; j++) if (tempscores[j] < tempscores[j + 1]) {
				temp = tempscores[j];
				tempscores[j] = tempscores[j + 1];
				tempscores[j + 1] = temp;
				swaps++;
			}
		} while (swaps > 0);
		for (j = 0, y = 60 + (textheight(difftext[diff]) * 2); j < i; j++) {
			outtextxy(x, y, ultoa(tempscores[j], numtext, 10));
			y += textheight(difftext[diff]);
		}
		end:
		x += 180;
	}
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, x = 140; i < 6; i += 2) { // For this one, y serves as x
		outtextxy(x, 360, scorescho[i == choice * 2 ? i + 1 : i]);
		x += 180;
	}
	outtextxy(320, 360 + textheight(scorescho[i]), scorescho[i == choice * 2 ? i + 1 : i]);
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice >= 3) {
		choice = 1;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 3) {
		choice = 3;
		goto draw;
	} else if ((c == 'A' || c == 'a') && choice > 0 && choice < 3) {
		choice--;
		goto draw;
	} else if ((c == 'D' || c == 'd') && choice < 2) {
		choice++;
		goto draw;
	} else if (c == 13) {
		if (choice == 0) {
			sound(82.41);
			delay(125);
			nosound();
			fclose(scores[0]);
			scores[0] = fopen("hmscoeas.txt", "w+t"); // Replace with blank file
			goto draw;
		} else if (choice == 1) {
			sound(82.41);
			delay(125);
			nosound();
			fclose(scores[1]);
			scores[1] = fopen("hmscoint.txt", "w+t"); // Replace with blank file
			goto draw;
		} else if (choice == 2) {
			sound(82.41);
			delay(125);
			nosound();
			fclose(scores[2]);
			scores[2] = fopen("hmscohar.txt", "w+t"); // Replace with blank file
			goto draw;
		} else {
			sound(82.41);
			delay(125);
			nosound();
			cleardevice();
			return;
		}
	} else goto choose;
}

void help() { // Instructions
	int i, y;
	char c;
	drawbg(LIGHTGREEN);
	settextjustify(1, 2);
	settextstyle(3, HORIZ_DIR, 1);
	for (i = 0, y = 60; i < 10; i++) {
		outtextxy(320, y, helptext[i]);
		y += textheight(helptext[i]);
	}
	outtextxy(320, 360, "[BACK]");
	choose:
	c = getch();
	if (c == 13) {
		sound(82.41);
		delay(125);
		nosound();
		cleardevice();
		return;
	} else goto choose;
}

void game(int diff) { // Hangman itself
	// Gotos may suck, but for this situation, it's good for testing draws
	// Possibility of word repetition is left untouched
	// 	due to the dynamic nature of the lex files
	// Extra lives happen on level 10n + 1
	int lives, hit, rng, i, j, y, wordlen, swaps;
	// lives is self-explanatory
	// hit is for knowing if you sucked or not
	// rng is for storing the RNGed index
	// swaps is for the bubble sort
	unsigned long level, tempscores[6], temp;
	// "level", because "round" seemed differently
	// 	differently highlighted in my text editor
	// tempscores is for storing and sorting score file input
	// temp is for the bubble sort
	char input, word[60], current[60], board1[25], board2[25];
	char livestext[15], leveltext[60], numtext[10];
	// input is for getch()
	// word is the RNGed choice from the lexicon
	// current is used for displaying the incomplete word
	// board1 and board2 are for displaying the available letters
	// livestext is for displaying how hopeless you are
	// leveltext is dual-purpose, for displaying level and death text
	// numtext is for storing ultoa and itoa
	start: // New game
	level = 0; // Start with 0 so the first newlevel iteration starts with 1
	lives = 3; // Start the game with 3 lives

	newlevel: // New round
	strcpy(board1, "A B C D E F G H I J K L M");
	strcpy(board2, "N O P Q R S T U V W X Y Z");
	level++;
	rng = random(lexcount[diff]) + 1;
	rewind(lex[diff]);
	i = 0;
	for (i = 0; i < rng; i++) fgets(word, 60, lex[diff]);
	for (i = 0; word[i] != '\n' && word[i] != '\0'; i++) current[i] = (word[i] == ' ') ? ' ' : 7;
	word[i] = '\0';
	current[i] = '\0';
	wordlen = strlen(word);

	draw: // Draw the game
	cleardevice();
	strcpy(leveltext, "LEVEL ");
	strcat(leveltext, ultoa(level, numtext, 10));
	strcpy(livestext, "LIVES: ");
	strcat(livestext, itoa(lives, numtext, 10));
	if (lives >= 3) setcolor(LIGHTGREEN);
	else if (lives == 2) setcolor(YELLOW);
	else if (lives == 1) setcolor(LIGHTRED);
	else goto died;
	// Draw the boards, blanks and stats
	settextjustify(1, 2);
	settextstyle(0, HORIZ_DIR, 2);
	outtextxy(480, 120, difftext[diff]);
	outtextxy(480, 140, leveltext);
	outtextxy(480, 160, livestext);
	outtextxy(320, 420, board1);
	outtextxy(320, 440, board2);
	if (strlen(word) > 30) settextstyle(0, HORIZ_DIR, 1);
	outtextxy(320, 360, current);
	// Draw the stickman guy
	// Head
	circle(205, 100, 30);
	// Torso
	line(205, 130, 205, 190);
	// Legs
	line(205, 190, 180, 260);
	line(205, 190, 230, 260);
	// Arms
	line(205, 135, 170, 150);
	line(205, 135, 240, 150);
	// Gallows	
	line(80, 280, 280, 280);
	line(120, 280, 120, 40);
	line(120, 80, 160, 40);
	line(120, 40, 205, 40);
	line(205, 40, 205, 70);
	if (strcmp(word, current) == 0) {
		settextstyle(0, HORIZ_DIR, 2);
		outtextxy(480, 180, "Success! Press any");
		outtextxy(480, 200, "key to continue.");
		getch();
		goto newlevel;
	}

	ready: // getch and check
	hit = 0;
	input = getch();
	if (input == 27) goto quitcho;
	input = toupper(input); // Set to uppercase
	// Check if invalid or previously done input
	if (input < 'A' || input > 'Z') goto ready;
	if (input >= 'A' && input <= 'M') {
		for (i = 0; i < 25; i += 2) {
			if (input == 'A' + (i / 2) && board1[i] == '_') goto ready;
			else if (input == board1[i]) {
				for (j = 0; j < wordlen; j++) {
					if (input == word[j]) {
						hit++;
						break;
					}
				}
				board1[i] = '_';
				break;
			}
		}
	} else {
		for (i = 0; i < 25; i += 2) {
			if (input == 'N' + (i / 2) && board2[i] == '_') goto ready;
			else if (input == board2[i]) {
				for (j = 0; j < wordlen; j++) {
					if (input == word[j]) {
						hit++;
						break;
					}
				}
				board2[i] = '_';
				break;
			}
		}
	}
	if (hit <= 0) lives--;
	else for (i = 0; i < wordlen; i++) if (input == word[i]) current[i] = word[i];
	if (strcmp(word, current) == 0) {
		if ((level + 1) % 10 == 1) {
			sound(2093);
			delay(200);
			nosound();
			lives++;
		} else {
			sound(1046.5);
			delay(200);
			nosound();
		}
	} else if (lives <= 0) {
		sound(41.21);
		delay(125);
		nosound();
		goto died;
	} else {
		if (hit > 0) sound(523.25);
		else sound(82.41);
		delay(125);
		nosound();
	}
	goto draw;

	quitcho:
	cleardevice();
	setcolor(WHITE);
	settextjustify(1, 2);
	settextstyle(SANS_SERIF_FONT, HORIZ_DIR, 2);
	y = 240;
	outtextxy(320, y, "Are you sure you want to");
	y += textheight(" ");
	outtextxy(320, y, "return to the main menu? [Y/N]");
	quitchoose:
	input = getch();
	if (input == 'Y' || input == 'y') return;
	else if (input == 'N' || input == 'n') goto draw;
	else goto quitchoose;

	died:
	rewind(scores[diff]);
	if (fgets(numtext, 10, scores[diff]) == NULL && level > 1) fprintf(scores[diff], "%lu\n", level - 1);
	else if (level > 1) {
		rewind(scores[diff]);
		for (i = 0; fgets(numtext, 10, scores[diff]) != NULL; i++) {
			j = sscanf(numtext, "%lu", &tempscores[i]); // Read each line from score file
			if (j <= 0) goto diedchoose; // Format break detected
		}
		tempscores[i] = level - 1; // Add current score to tempscores
		do { // Lol, bubble sort.
			swaps = 0;
			for (j = 0; j < i; j++) if (tempscores[j] < tempscores[j + 1]) {
				temp = tempscores[j];
				tempscores[j] = tempscores[j + 1];
				tempscores[j + 1] = temp;
				swaps++;
			}
		} while (swaps > 0);
		fclose(scores[diff]);
		if (diff == 0) scores[diff] = fopen("hmscoeas.txt", "w+t"); // Erase the damn file.
		else if (diff == 1) scores[diff] = fopen("hmscoint.txt", "w+t");
		else scores[diff] = fopen("hmscohar.txt", "w+t");
		if (i > 4) i = 4; // It works.
		for (j = 0; j < i + 1; j++) fprintf(scores[diff], "%lu\n", tempscores[j]);
	}
	cleardevice();
	settextjustify(1, 1);
	settextstyle(1, HORIZ_DIR, 16);
	setcolor(RED);
	y = 200;
	outtextxy(320, y, "HE DIED");
	y += 100;
	settextstyle(SANS_SERIF_FONT, HORIZ_DIR, 1);
	strcpy(leveltext, "You have completed ");
	strcat(leveltext, ultoa(level - 1, numtext, 10));
	strcat(leveltext, " round");
	if (level - 1 != 1) strcat(leveltext, "s");
	strcat(leveltext, ". Play again? [Y/N]");
	setcolor(WHITE);
	outtextxy(320, y, leveltext);
	diedchoose:
	input = getch();
	if (input == 'Y' || input == 'y') goto start;
	else if (input == 'N' || input == 'n') return;
	else goto diedchoose;
}

void drawbg(int color) { // Draw menu background
	int i, y;
	setcolor(color);
	cleardevice();
	line(640, 240, 400, 480);
	circle(320, 240, 300);
	settextjustify(0, 2);
	settextstyle(3, HORIZ_DIR, 2);
	setcolor(WHITE);
	for (i = 0, y = 400; i < 2; i++) {
		outtextxy(40, y, instr[i]);
		y += textheight(instr[i]);
	}
}

void drawtitle(int color) {
	int i, y;
	settextjustify(1, 2);
	settextstyle(0, HORIZ_DIR, 1);
	setcolor(color);
	for (i = 0, y = 120; i < 10; i++) {
		outtextxy(320, y, titleu[i]);
		y += textheight(titleu[i]);
	}
	setcolor(WHITE);
	for (i = 0, y = 120; i < 10; i++) {
		outtextxy(320, y, title[i]);
		y += textheight(title[i]);
	}
}

void newlex(FILE **lex, char file[], int diff) {
	int i;
	*lex = fopen(file, "w+t");
	if (diff == 0) for (i = 0; i < 30; i++) fprintf(*lex, "%s\n", newlexeasy[i]);
	else if (diff == 1) for (i = 0; i < 30; i++) fprintf(*lex, "%s\n", newlexintermed[i]);
	else for (i = 0; i < 30; i++) fprintf(*lex, "%s\n", newlexhard[i]);
	lexcount[diff] = i;
}

void openlex(FILE **lex, char file[]) {
	int diff, i, y;
	char word[60], c;
	if (strcmp(file, "hmlexeas.txt") == 0) diff = 0;
	else if (strcmp(file, "hmlexint.txt") == 0) diff = 1;
	else diff = 2;
	*lex = fopen(file, "a+t");
	if (*lex == NULL || fgets(word, 60, *lex) == NULL) newlex(&*lex, file, diff);
	else {
		rewind(*lex);
		while (fgets(word, 60, *lex) != NULL) {
			for (i = 0; word[i] != '\n' && word[i] != '\0'; i++) {
				if (word[i] != ' ' && (word[i] < 'A' || word[i] > 'Z') || i > 60) {
					cleardevice();
					settextjustify(1, 2);
					settextstyle(SANS_SERIF_FONT, HORIZ_DIR, 2);
					y = 240;
					outtextxy(320, y, lwrong[diff]);
					y += textheight(lwrong[diff]);
					outtextxy(320, y, lwrong[3]);
					choice:
					c = getch();
					if (c == 'Y' || c == 'y') {
						newlex(&*lex, file, diff);
						return;
					}
					else if (c == 'N' || c == 'n') exit(0);
					else goto choice;
				}
			}
			lexcount[diff]++;
		}
	}
}